# Package registry repository
Package registry for https://github.com/esantix/pip-package-template

For public use


## For requirements file use:
```text
--extra-index https://pypi-public:gldt-zUAbH1zBBmzmVJcjzcCE@gitlab.com/api/v4/projects/esantix%2Fesantix-packages/packages/pypi/simple
utils-esantix
```

## For pip3 command
```bash
pip3 install --extra-index https://pypi-public:gldt-zUAbH1zBBmzmVJcjzcCE@gitlab.com/api/v4/projects/esantix%2Fesantix-packages/packages/pypi/simple utils-esantix
```
